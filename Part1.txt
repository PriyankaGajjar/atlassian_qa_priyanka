1. What is writeable user?
2. Which other parameters in 'My Profile' can user change?
3. Are there any restriction for changing password like 'last password should not be repeated'?
4. Are there any validation messages for changed fields in profile?
5. What is difference between 'create user' and 'Manage Users'?
6. If admin changes password of a user ,will there be any automatic notifications to userregarding password change?